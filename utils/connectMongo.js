import mongoose from 'mongoose'

const connectMongo = async () => {
    try {
        mongoose.connect(process.env.MONGO_URL)
    } catch (error) {
        console.log(error)
    }
}

export default connectMongo
