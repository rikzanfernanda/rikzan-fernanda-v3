import connectMongo from '../../../utils/connectMongo'
import User from '../../../models/UserModel'

async function getUser(req, res) {
    try {
        await connectMongo()

        const user = await User.findOne({
            username: 'rikzanfernanda'
        })

        res.status(200).json({
            success: true,
            message: '',
            data: user
        })
    } catch (error) {
        res.status(500).json({
            success: false,
            message: error.message,
            data: []
        })
    }
}

export default getUser
