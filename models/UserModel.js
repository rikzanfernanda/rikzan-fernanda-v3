import mongoose, { models, Schema } from 'mongoose'

const userSchema = new Schema({
    username: {
        type: String
    },
    name: {
        type: String
    },
    resume: {
        type: String
    }
})

const User = models.user || mongoose.model('user', userSchema)

export default User
