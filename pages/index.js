import Head from 'next/head'

export default function Home() {
    return (
        <>
            <Head>
                <title>Rikzan Fernanda</title>
                <meta
                    name="description"
                    content="Rikzan Fernanda's personal website"
                />
                <meta
                    name="keywords"
                    content="Rikzan, Fernanda, Rikzan Fernanda, portfolio, personal website, portfolio website, personal portfolio, personal portfolio website, rikzan fernanda portfolio, rikzan fernanda personal website, rikzan fernanda portfolio website, rikzan fernanda personal portfolio, rikzan fernanda personal portfolio website"
                />
                <meta name="author" content="Rikzan Fernanda" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <h1 className='text-secondary'>Home</h1>
        </>
    )
}
